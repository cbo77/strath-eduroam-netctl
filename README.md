# Eduroam netctl unit

sample netctl profile for eduroam at Strathclyde

## Installation

Edit the strath-eduroam-example file, inputting your details as necessary.
You will need to point it to the location of certificates for eduroam, these are
provided by strath from http://ca.strath.ac.uk/ or can be extracted from the
eduroam CAT installer available from https://cat.eduroam.org/?idp=140

The installer contains the certificate in a string near the bottom of
the script. Its easier I reckon to `curl` it from the former however:

```bash
curl http://ca.strath.ac.uk/strathNetRootCA1.crt > ~/ca.pem
```

Then, you just copy it to the netctl units dir

```bash
sudo cp strath-eduroam-example /etc/netctl/.
```

and activate it.
